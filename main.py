import tweepy
import json
import time

from twitter_timeline import get_twitter_timeline
from twitter_follow import follow_selected_follower
from tweet_like import like_tweets_by_keyword


try:
    with open("secrets.json") as json_file:
        secrets = json.load(json_file)
except Exception as ex:
    print(ex)

auth_secrets_for_api = secrets["api"][0]

auth = tweepy.OAuthHandler(auth_secrets_for_api["key"], auth_secrets_for_api["secret"])
auth.set_access_token(auth_secrets_for_api["access_token"], auth_secrets_for_api["access_token_secret"])

api = tweepy.API(auth)

get_twitter_timeline(api)

follow_selected_follower("xxx", api)

like_tweets_by_keyword("Bitcoin", 2, api)





