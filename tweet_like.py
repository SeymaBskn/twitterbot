import tweepy

def like_tweets_by_keyword(keyword, tweet_number, authenticated_api):
    for tweet in tweepy.Cursor(authenticated_api.search, keyword).items(tweet_number):
        try:
            tweet.favorite()
            print("I liked this tweet")
        except tweepy.TweepError as ex:
            print(ex.reason)
        except StopIteration:
            break