import tweepy
import time

def follow_selected_follower(follower_name, authenticated_api):
    while True:
        try:
            for follower in tweepy.Cursor(authenticated_api.followers).items():
                if follower.name == follower_name:
                    follower.follow()
                    break
        except tweepy.RateLimitError as ex:
            time.sleep(300)