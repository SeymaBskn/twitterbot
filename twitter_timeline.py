import tweepy

def get_twitter_timeline(authenticated_api):
    public_tweets = authenticated_api.home_timeline()
    for tweet in public_tweets:
        print(tweet.text)